################################################################################
#
# Author: 	Benjamin Zinschitz
# Date:		08.07.2020
#
# Description:
# 	A Makefile for building the Chip-8 interpreter
#
################################################################################

# The generated executable
TARGET = chip_8

# How to compile
CC = gcc
CFLAGS = -std=c99 -Wall -c -g -pg
LFLAGS = -Wall -lSDL2 -lm -pg

# Where to find things
SRC_DIR = ./src
BUILD_DIR = ./build

SRCS := $(wildcard $(SRC_DIR)/*.c)
INCS := $(wildcard $(SRC_DIR)/*.h)
OBJS := $(SRCS:$(SRC_DIR)/%.c=$(BUILD_DIR)/%.o)
ASMS := $(SRCS:$(SRC_DIR)/%.c=$(BUILD_DIR)/%.s)

# Targets for executable
all: $(TARGET)

debug:	CFLAGS += -DDEBUG
debug:	$(TARGET)

$(TARGET):	$(OBJS)
	$(CC) $(OBJS) $(LFLAGS) -o $@

$(OBJS):	$(BUILD_DIR)/%.o:	$(SRC_DIR)/%.c
	mkdir -p $(BUILD_DIR)
	$(CC) $(CFLAGS) $< -o $@

.PHONY: clean
clean:
	rm -rf $(BUILD_DIR) $(TARGET)
