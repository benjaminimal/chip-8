# Chip 8

## What this is

This is just another implementation of the [Chip-8](https://en.wikipedia.org/wiki/CHIP-8) interpreter for my own education and fun.

## How to build

As a note this has been compiled and run on Debian 10 only so I can not guarantee anything for other systems.  

You will need the following to build it yourself.

- [gcc](https://gcc.gnu.org/)
- [SDL 2](https://www.libsdl.org/)

After cloning the repository and installing these prequesites you want to compile with
```bash
make
```
which should leave the `chip_8` executable in the current directory.

## How to use

You can run the program like this.
```bash
./chip_8 ROM_PATH
```
Some roms are provided in the `roms/chip-8/` directory.

The system has a 16 button keypad which is mapped to the keyboard in the following way.
```
    ORIGINAL       =>         NEW
|---|---|---|---|      |---|---|---|---|
| 1 | 2 | 3 | C |  =>  | 1 | 2 | 3 | 4 |
|---|---|---|---|      |---|---|---|---|
| 4 | 5 | 6 | D |  =>  | Q | W | E | R |
|---|---|---|---|      |---|---|---|---|
| 7 | 8 | 9 | E |  =>  | A | S | D | F |
|---|---|---|---|      |---|---|---|---|
| A | 0 | B | F |  =>  | Z | X | C | V |
|---|---|---|---|      |---|---|---|---|
```

## Ressources

- [Technical Specification](http://devernay.free.fr/hacks/chip8/C8TECH10.HTM)
