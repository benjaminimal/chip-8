#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#include "chip8.h"

/* Macros */

#define TST_OP_EQ(opcode, mask, test) \
  ((opcode) & (mask)) == test

#define MASK_ADDR(opcode) \
  ((opcode) & 0x0FFF)

#define MASK_NIBBLE(opcode) \
  ((opcode) & 0x000F)

#define MASK_X(opcode) \
  (((opcode) & 0x0F00) >> 8)

#define MASK_Y(opcode) \
  (((opcode) & 0x00F0) >> 4)

#define MASK_BYTE(opcode) \
  ((opcode) & 0x00FF)

#define READ_X(machine, opcode) \
  (machine)->V[MASK_X(opcode)]

#define WRITE_X(machine, opcode, value) \
  (machine)->V[MASK_X(opcode)] = value

#define READ_Y(machine, opcode) \
  (machine)->V[MASK_Y(opcode)]

#define WRITE_Y(machine, opcode, value) \
  (machine)->V[MASK_Y(opcode)] = value

#define KEY_DOWN(machine, key) \
  (machine)->is_key_down(key)

#define READ_MEM(machine, address) \
  (machine)->memory[address]

#define WRITE_MEM(machine, address, value) \
  (machine)->memory[address] = value

#define GET_OPCODE(machine) \
  (READ_MEM((machine), (machine)->pc) << 8) | READ_MEM((machine), (machine)->pc + 1)

#define CLEAR_DISPLAY(machine) \
  memset((machine)->display, 0, C8_DSP_BND * sizeof(uint8_t)); \
  (machine)->redraw = 1


/* Globals */

const static uint8_t C8_HEX_FONT[C8_HEX_FONT_SIZE] = {
  0xF0, 0x90, 0x90, 0x90, 0xF0, /* 1 */
  0x20, 0x60, 0x20, 0x20, 0x70, /* 2 */
  0xF0, 0x10, 0xF0, 0x80, 0xF0, /* 3 */
  0xF0, 0x10, 0xF0, 0x10, 0xF0, /* 4 */
  0x90, 0x90, 0xF0, 0x10, 0x10, /* 5 */
  0xF0, 0x80, 0xF0, 0x10, 0xF0, /* 6 */
  0xF0, 0x80, 0xF0, 0x90, 0xF0, /* 7 */
  0xF0, 0x10, 0x20, 0x40, 0x40, /* 8 */
  0xF0, 0x90, 0xF0, 0x90, 0xF0, /* 9 */
  0xF0, 0x90, 0xF0, 0x10, 0xF0, /* 0 */
  0xF0, 0x90, 0xF0, 0x90, 0x90, /* A */
  0xE0, 0x90, 0xE0, 0x90, 0xE0, /* B */
  0xF0, 0x80, 0x80, 0x80, 0xF0, /* C */
  0xE0, 0x90, 0x90, 0x90, 0xE0, /* D */
  0xF0, 0x80, 0xF0, 0x80, 0xF0, /* E */
  0xF0, 0x80, 0xF0, 0x80, 0x80  /* F */
};


/* Interface function definitions */

void chip8_init(chip8_t *machine, key_poller_t *key_poller) {
  /* No need to initialize memory or stack as they should be written first */

  srand(time(0));       /* Initialize rng */

  machine->sp = -1;      /* Nothing to return to */
  machine->pc = C8_START_EXE_ADDR;  /* Most programs would start at address 0x200 */
  machine->I = 0;       /* No saved address to start with */

  machine->DT = 0;      /* Delay timer not active */
  machine->ST = 0;      /* No sound to start with */

  machine->cycles = 0;  /* Nothing has been executed so far */

  memset(machine->V, 0, C8_V_BND * sizeof(uint8_t)); /* All registers set to 0 */

  machine->is_key_down = key_poller;  /* This is how we check the key states */

  machine->blocked = 0;       /* Not waiting for a key press */
  machine->V_waiting = NULL;  /* Not waiting for a key press */

  memcpy(machine->memory, C8_HEX_FONT, C8_HEX_FONT_SIZE * sizeof(uint8_t)); /* Copy hex font to memory */

  CLEAR_DISPLAY(machine);   /* Let's start with a blank screen */
}

int chip8_load(chip8_t *machine, uint8_t *rom_buffer, int bytes) {
  // Check the rom size
  if (bytes > C8_MAX_ROM_SIZE) { return -1; }

  // Copy rom to memory
  memcpy(&machine->memory[C8_START_EXE_ADDR], rom_buffer, bytes);

  return 0;
}

int chip8_step(chip8_t *machine) {
  // Clear the updated status of the screen
  machine->redraw = 0;
  
  // Fetch and decode opcode
  uint16_t opcode = GET_OPCODE(machine);

  /* The machine is blocked and will unblock on key down */
  if (machine->blocked) {
    for (uint8_t i = 0; i < C8_KEY_BND; i++) {
      if (KEY_DOWN(machine, i)) {
        *machine->V_waiting = i;
        machine->blocked = 0;
      }
    }
  }
  /* The machine is not blocked and will thus execute */
  if (!machine->blocked) {
    // Increment program counter
    machine->pc += 2;
    /**
     * Execute instruction
     *
     * In these listings, the following variables are used:
     * nnn or addr - A 12-bit value, the lowest 12 bits of the instruction     0x0FFF
     * n or nibble - A 4-bit value, the lowest 4 bits of the instruction       0x000F
     * x - A 4-bit value, the lower 4 bits of the high byte of the instruction 0x0F00
     * y - A 4-bit value, the upper 4 bits of the low byte of the instruction  0x00F0
     * kk or byte - An 8-bit value, the lowest 8 bits of the instruction       0x00FF
     */
    if (0) {} /* This is just here to unify the following else if statements.
                 The compiler should remove this when optimizing */
    /* 0nnn | SYS addr | Jump to a machine code routine at nnn */
    /* This instruction is only used on the old computers on which Chip-8 was originally implemented. It is ignored by modern interpreters */
    /* else if (TST_OP_EQ(opcode, 0xF000, 0x0000)) {} // How can this one not eat 0x00EE or 0x00E0 ?? */ 
    /* 00E0 | CLS | Clear the display */
    else if (TST_OP_EQ(opcode, 0xFFFF, 0x00E0)) {
      CLEAR_DISPLAY(machine);
    }
    /* 00EE | RET | Return from a subroutine */
    /* The interpreter sets the program counter to the address at the top of the stack, then subtracts 1 from the stack pointer */
    else if (TST_OP_EQ(opcode, 0xFFFF, 0x00EE)) {
      machine->pc = machine->stack[machine->sp--];
    }
    /* 1nnn | JP addr | Jump to location nnn */
    /* The interpreter sets the program counter to nnn */
    else if (TST_OP_EQ(opcode, 0xF000, 0x1000)) {
      machine->pc = MASK_ADDR(opcode);
    }
    /* 2nnn | CALL addr | Call subroutine at nnn */
    /* The interpreter increments the stack pointer, then puts the current PC on the top of the stack. The PC is then set to nnn */
    else if (TST_OP_EQ(opcode, 0xF000, 0x2000)) {
      machine->stack[++machine->sp] = machine->pc;
      machine->pc = MASK_ADDR(opcode);
    }
    /* 3xkk | SE Vx, byte | Skip next instruction if Vx = kk */
    /* The interpreter compares register Vx to kk, and if they are equal, increments the program counter by 2 */
    else if (TST_OP_EQ(opcode, 0xF000, 0x3000)) {
      if (READ_X(machine, opcode) == MASK_BYTE(opcode)) {
        machine->pc += 2;
      }
    }
    /* 4xkk | SNE Vx, byte | Skip next instruction if Vx != kk */
    /* The interpreter compares register Vx to kk, and if they are not equal, increments the program counter by 2 */
    else if (TST_OP_EQ(opcode, 0xF000, 0x4000)) {
      if (READ_X(machine, opcode) != MASK_BYTE(opcode)) {
        machine->pc += 2;
      }
    }
    /* 5xy0 | SE Vx, Vy | Skip next instruction if Vx = Vy */
    /* The interpreter compares register Vx to register Vy, and if they are equal, increments the program counter by 2 */
    else if (TST_OP_EQ(opcode, 0xF00F, 0x5000)) {
      if (READ_X(machine, opcode) == READ_Y(machine, opcode)) {
        machine->pc += 2;
      }
    }
    /* 6xkk | LD Vx, byte | Set Vx = kk */
    /* The interpreter puts the value kk into register Vx */
    else if (TST_OP_EQ(opcode, 0xF000, 0x6000)) {
      WRITE_X(machine, opcode, MASK_BYTE(opcode));
    }
    /* 7xkk | ADD Vx, byte | Set Vx = Vx + kk */
    /* Adds the value kk to the value of register Vx, then stores the result in Vx */
    else if (TST_OP_EQ(opcode, 0xF000, 0x7000)) {
      WRITE_X(machine, opcode, READ_X(machine, opcode) + MASK_BYTE(opcode));
    }
    /* 8xy0 | LD Vx, Vy | Set Vx = Vy */
    /* Stores the value of register Vy in register Vx */
    else if (TST_OP_EQ(opcode, 0xF00F, 0x8000)) {
      WRITE_X(machine, opcode, READ_Y(machine, opcode));
    }
    /* 8xy1 | OR Vx, Vy | Set Vx = Vx OR Vy */
    /* Performs a bitwise OR on the values of Vx and Vy, then stores the result in Vx */
    else if (TST_OP_EQ(opcode, 0xF00F, 0x8001)) {
      WRITE_X(machine, opcode, READ_X(machine, opcode) | READ_Y(machine, opcode));
    }
    /* 8xy2 | AND Vx, Vy | Set Vx = Vx AND Vy */
    /* Performs a bitwise AND on the values of Vx and Vy, then stores the result in Vx */
    else if (TST_OP_EQ(opcode, 0xF00F, 0x8002)) {
      WRITE_X(machine, opcode, READ_X(machine, opcode) & READ_Y(machine, opcode));
    }
    /* 8xy3 | XOR Vx, Vy | Set Vx = Vx XOR Vy */
    /* Performs a bitwise exclusive OR on the values of Vx and Vy, then stores the result in Vx */
    else if (TST_OP_EQ(opcode, 0xF00F, 0x8003)) {
      WRITE_X(machine, opcode, READ_X(machine, opcode) ^ READ_Y(machine, opcode));
    }
    /* 8xy4 | ADD Vx, Vy | Set Vx = Vx + Vy, set VF = carry */
    /* The values of Vx and Vy are added together. If the result is greater than 8 bits (i.e., > 255,) VF is set to 1, otherwise 0. Only the lowest 8 bits of the result are kept, and stored in Vx */
    else if (TST_OP_EQ(opcode, 0xF00F, 0x8004)) {
      machine->V[0xF] = READ_X(machine, opcode) + READ_Y(machine, opcode) > 0xFF ? 0x01 : 0x00;
      WRITE_X(machine, opcode, READ_X(machine, opcode) + READ_Y(machine, opcode));
    }
    /* 8xy5 | SUB Vx, Vy | Set Vx = Vx - Vy, set VF = NOT borrow */
    /* If Vx > Vy, then VF is set to 1, otherwise 0. Then Vy is subtracted from Vx, and the results stored in Vx */
    else if (TST_OP_EQ(opcode, 0xF00F, 0x8005)) {
      machine->V[0xF] = READ_X(machine, opcode) > READ_Y(machine, opcode) ? 0x01 : 0x00;
      WRITE_X(machine, opcode, READ_X(machine, opcode) - READ_Y(machine, opcode));
    }
    /* 8xy6 | SHR Vx {, Vy} | Set Vx = Vx SHR 1 */
    /* If the least-significant bit of Vx is 1, then VF is set to 1, otherwise 0. Then Vx is divided by 2 */
    else if (TST_OP_EQ(opcode, 0xF00F, 0x8006)) {
      machine->V[0xF] = READ_X(machine, opcode) & 0x01;
      WRITE_X(machine, opcode, READ_X(machine, opcode) >> 1);
    }
    /* 8xy7 | SUBN Vx, Vy | Set Vx = Vy - Vx, set VF = NOT borrow */
    /* If Vy > Vx, then VF is set to 1, otherwise 0. Then Vx is subtracted from Vy, and the results stored in Vx */
    else if (TST_OP_EQ(opcode, 0xF00F, 0x8007)) {
      machine->V[0xF] = READ_Y(machine, opcode) > READ_X(machine, opcode) ? 0x01 : 0x00;
      WRITE_X(machine, opcode, READ_Y(machine, opcode) - READ_X(machine, opcode));
    }
    /* 8xyE | SHL Vx {, Vy} | Set Vx = Vx SHL 1 */
    /* If the most-significant bit of Vx is 1, then VF is set to 1, otherwise to 0. Then Vx is multiplied by 2 */
    else if (TST_OP_EQ(opcode, 0xF00F, 0x800E)) {
      machine->V[0xF] = READ_X(machine, opcode) & 0x80 ? 0x01 : 0x00;
      WRITE_X(machine, opcode, READ_X(machine, opcode) << 1);
    }
    /* 9xy0 | SNE Vx, Vy | Skip next instruction if Vx != Vy */
    /* The values of Vx and Vy are compared, and if they are not equal, the program counter is increased by 2 */
    else if (TST_OP_EQ(opcode, 0xF00F, 0x9000)) {
      if (READ_X(machine, opcode) != READ_Y(machine, opcode)) {
        machine->pc += 2;
      }
    }
    /* Annn | LD I, addr | Set I = nnn */
    /* The value of register I is set to nnn */
    else if (TST_OP_EQ(opcode, 0xF000, 0xA000)) {
      machine->I = MASK_ADDR(opcode);
    }
    /* Bnnn | JP V0, addr | Jump to location nnn + V0 */
    /* The program counter is set to nnn plus the value of V0 */
    else if (TST_OP_EQ(opcode, 0xF000, 0xB000)) {
      machine->pc = MASK_ADDR(opcode) + machine->V[0];
    }
    /* Cxkk | RND Vx, byte | Set Vx = random byte AND kk */
    /* The interpreter generates a random number from 0 to 255, which is then ANDed with the value kk. The results are stored in Vx. See instruction 8xy2 for more information on AND */
    else if (TST_OP_EQ(opcode, 0xF000, 0xC000)) {
      uint8_t random = rand() % 0x100;
      WRITE_X(machine, opcode, random & MASK_BYTE(opcode));
    }
    /* Dxyn | DRW Vx, Vy, nibble | Display n-byte sprite starting at memory location I at (Vx, Vy), set VF = collision */
    /* The interpreter reads n bytes from memory, starting at the address stored in I. These bytes are then displayed as sprites on screen at coordinates (Vx, Vy). Sprites are XORed onto the existing screen. If this causes any pixels to be erased, VF is set to 1, otherwise it is set to 0. If the sprite is positioned so part of it is outside the coordinates of the display, it wraps around to the opposite side of the screen. */
    else if (TST_OP_EQ(opcode, 0xF000, 0xD000)) {
      uint16_t n = MASK_NIBBLE(opcode);
      uint8_t x = READ_X(machine, opcode);
      uint8_t y = READ_Y(machine, opcode);
      uint8_t *src = &machine->memory[machine->I];
      machine->V[0xF] = 0;
      for (int i = 0; i < n; i++) {
        uint16_t mask = 0x80;
        for (int j = 0; j < 8; j++) {
          uint8_t write_buff = (src[i] & (mask >> j)) ? 0xFF : 0x00;
          uint8_t *dst = &machine->display[(y + i) % C8_DSP_HGT][((x + j) % C8_DSP_WDT) * C8_DSP_BPP];
          machine->V[0xF] = write_buff & *dst ? 0x01 : machine->V[0xF];
          for (int k = 0; k < C8_DSP_BPP; k++) {
            dst[k] ^= write_buff;
          }
        }
      }
      machine->redraw = 1;
    }
    /* Ex9E | SKP Vx | Skip next instruction if key with the value of Vx is pressed */
    /* Checks the keyboard, and if the key corresponding to the value of Vx is currently in the down position, PC is increased by 2 */
    else if (TST_OP_EQ(opcode, 0xF0FF, 0xE09E)) {
      if (KEY_DOWN(machine, READ_X(machine, opcode)) == 1) {
        machine->pc += 2;
      }
    }
    /* ExA1 | SKNP Vx | Skip next instruction if key with the value of Vx is not pressed */
    /* Checks the keyboard, and if the key corresponding to the value of Vx is currently in the up position, PC is increased by 2 */
    else if (TST_OP_EQ(opcode, 0xF0FF, 0xE0A1)) {
      if (KEY_DOWN(machine, READ_X(machine, opcode)) == 0) {
        machine->pc += 2;
      }
    }
    /* Fx07 | LD Vx, DT | Set Vx = delay timer value */
    /* The value of DT is placed into Vx */
    else if (TST_OP_EQ(opcode, 0xF0FF, 0xF007)) {
      WRITE_X(machine, opcode, machine->DT);
    }
    /* Fx0A | LD Vx, K | Wait for a key press, store the value of the key in Vx */
    /* All execution stops until a key is pressed, then the value of that key is stored in Vx */
    else if (TST_OP_EQ(opcode, 0xF0FF, 0xF00A)) {
      machine->blocked = 1;
      machine->V_waiting = &machine->V[MASK_X(opcode)];
    }
    /* Fx15 | LD DT, Vx | Set delay timer = Vx */
    /* DT is set equal to the value of Vx */
    else if (TST_OP_EQ(opcode, 0xF0FF, 0xF015)) {
      machine->DT = READ_X(machine, opcode);
    }
    /* Fx18 | LD ST, Vx | Set sound timer = Vx */
    /* ST is set equal to the value of Vx */
    else if (TST_OP_EQ(opcode, 0xF0FF, 0xF018)) {
      machine->ST = READ_X(machine, opcode);
    }
    /* Fx1E | ADD I, Vx | Set I = I + Vx */
    /* The values of I and Vx are added, and the results are stored in I */
    else if (TST_OP_EQ(opcode, 0xF0FF, 0xF01E)) {
      machine->I += READ_X(machine, opcode);
    }
    /* Fx29 | LD F, Vx | Set I = location of sprite for digit Vx */
    /* The value of I is set to the location for the hexadecimal sprite corresponding to the value of Vx. */
    else if (TST_OP_EQ(opcode, 0xF0FF, 0xF029)) {
      machine->I = READ_X(machine, opcode) * C8_HEX_CHAR_SIZE;
    }
    /* Fx33 | LD B, Vx | Store BCD representation of Vx in memory locations I, I+1, and I+2 */
    /* The interpreter takes the decimal value of Vx, and places the hundreds digit in memory at location in I, the tens digit at location I+1, and the ones digit at location I+2 */
    else if (TST_OP_EQ(opcode, 0xF0FF, 0xF033)) {
      WRITE_MEM(machine, machine->I + 0, READ_X(machine, opcode) / 100);
      WRITE_MEM(machine, machine->I + 1, (READ_X(machine, opcode) % 100) / 10);
      WRITE_MEM(machine, machine->I + 2, READ_X(machine, opcode) % 10);
    }
    /* Fx55 | LD [I], Vx | Store registers V0 through Vx in memory starting at location I */
    /* The interpreter copies the values of registers V0 through Vx into memory, starting at the address in I */
    else if (TST_OP_EQ(opcode, 0xF0FF, 0xF055)) {
      memcpy(
          &machine->memory[machine->I],
          &machine->V[0],
          (MASK_X(opcode) + 1) * sizeof(uint8_t)
      );
    }
    /* Fx65 | LD Vx, [I] | Read registers V0 through Vx from memory starting at location I */
    /* The interpreter reads values from memory starting at location I into registers V0 through Vx */
    else if (TST_OP_EQ(opcode, 0xF0FF, 0xF065)) {
      memcpy(
          &machine->V[0],
          &machine->memory[machine->I],
          (MASK_X(opcode) + 1) * sizeof(uint8_t)
      );
    }
    else {
      return -1;
    }
  }

  return 0;
}

void chip8_update_timers(chip8_t *machine) {
    if (machine->DT > 0) { machine->DT--; }
    if (machine->ST > 0) { machine->ST--; }
}

void chip8_destroy(chip8_t *machine) {
  // PLACEHOLDER: nothing to clean up yet
}

int chip8_is_display_updated(chip8_t *machine) {
  return machine->redraw;
}

int chip8_is_sound_active(chip8_t *machine) {
  return machine->ST > 0;
}

void chip8_coredump(chip8_t *machine) {

  // printf("===============================================\n");

  printf("====================MEMORY=====================\n");

  for (int i = 0, step = 16; i < C8_MEM_BND; i += step) {
    printf("%04X:\t", i);
    for (int j = 0; j < step; j += 2) {
      printf("%02X%02X ", machine->memory[i + j], machine->memory[i + j + 1]);
    }
    printf("\n");
  }

  printf("===REGISTERS========KEYS============STACK======\n");

  printf("cycles:\t%ld\n", machine->cycles);
  printf("blocked:\t%d\n", machine->blocked);
  printf("opcode:\t%04X\n", GET_OPCODE(machine));
  printf("PC:\t%04X\n", machine->pc);
  printf("SP:\t  %02X\n", machine->sp);
  printf("I:\t%04X\n", machine->I);
  printf("DT:\t  %02X\n", machine->DT);
  printf("ST:\t  %02X\n", machine->ST);
  for (int i = 0; i < C8_V_BND; i++) {
    printf("V%01X:\t  %02X\t", i, machine->V[i]);         /* registers */
    printf("Key%01X:\t%01d\t", i, KEY_DOWN(machine, i));  /* key state */
    printf("%01X:\t%04X\n", i, machine->stack[i]);        /* stack */
  }

  printf("===================DISPLAY=====================\n");

  for (int y = 0; y < C8_DSP_HGT; y++) {
    printf("%03X:\t", y * C8_DSP_WDT);
    for (int x = 0; x < C8_DSP_WDT; x++) {
      printf(machine->display[y][x * C8_DSP_BPP] ? "*" : "_");
    }
    printf("\n");
  }
}
