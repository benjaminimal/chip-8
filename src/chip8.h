#ifndef _H_CHIP8_
#define _H_CHIP8_

#include <stdint.h>


/* Constants */

#define C8_MEM_BND          0x1000
#define C8_STK_BND          0x10
#define C8_V_BND            0x10
#define C8_KEY_BND          0x10
#define C8_DSP_BPP          3
#define C8_DSP_WDT          64
#define C8_DSP_HGT          32
#define C8_DSP_BND          (C8_DSP_WDT * C8_DSP_BPP * C8_DSP_HGT)
#define C8_DLY_RTE          60
#define C8_HEX_CHAR_SIZE    5
#define C8_HEX_CHAR_COUNT   16
#define C8_HEX_FONT_SIZE    (C8_HEX_CHAR_SIZE * C8_HEX_CHAR_COUNT)
#define C8_MAX_ROM_SIZE     (C8_MEM_BND - C8_START_EXE_ADDR)
#define C8_START_EXE_ADDR   0x200


/* Data Structures */

typedef int (key_poller_t)(uint8_t);

/**
 * This should represent the whole Chip-8 system
 */
typedef struct {
    uint8_t memory[C8_MEM_BND];     /* The system has up to 4KB of RAM */
    uint16_t stack[C8_STK_BND];     /* And a stack with a depth of 16 */
    uint8_t sp;                     /* This is the stack pointer */
    uint16_t pc;                    /* Of course we need a program counter */
    uint16_t I;                     /* Special register to store memory addresses */
    uint8_t V[C8_V_BND];            /* And 16 8-bit general purpose registers */
    uint8_t DT;                     /* The delay timer register. Decrements at
                                       a rate of 60 Hz. Is active if non zero */
    uint8_t ST;                     /* The sound timer, a register to signal
                                       whether to beep. Will decrement at a 
                                       rate of 60 Hz */
    int64_t cycles;                 /* Counter to keep track of how many cpu
                                       cycles have passed */
    key_poller_t *is_key_down;      /* 16 keys as input */

    uint8_t display[C8_DSP_HGT][C8_DSP_WDT * C8_DSP_BPP];   /* A 64 * 32
                                                               monochrome grid
                                                               as output */
    uint8_t redraw;                 /* Indicator for screen updates */
    uint8_t blocked;                /* Indicate whether waiting for key event */
    uint8_t *V_waiting;             /* The register waiting for the key event */
} chip8_t;


/* Function interface */

/**
 * Use to create a fresh machine.
 * @param machine   A pointer to the machine to be initalized.
 */
void chip8_init(chip8_t *machine, key_poller_t *key_poller);

/**
 * Load a program into the machines memory to be executed.
 * @param machine       The machine to execute the rom.
 * @param rom_buffer    The buffer holding the rom data.
 * @param bytes         The number of bytes in rom_buffer.
 * @return              0 on success, < 0 on exeeded rom size.
 */
int chip8_load(chip8_t *machine, uint8_t *rom_buffer, int bytes);

/**
 * Step forward a single cpu cycle.
 * @param machine   The Chip-8 machine which should execute it's next instruction.
 * @return  0 on success, < 0 when an unknown opcode has been encountered.
 */
int chip8_step(chip8_t *machine);

/**
 * Decrements the machines timer registers.
 * @param machine   The machine on which the timers should be decremented.
 */
void chip8_update_timers(chip8_t *machine);

/**
 * Destroy the machine.
 */
void chip8_destroy(chip8_t *machine);

/**
 * Indicate whether the machines display has been updated in a previos step.
 * @param machine   The machine to probe.
 * @return          1 on an upadted display, 0 else.
 */
int chip8_is_display_updated(chip8_t *machine);

/**
 * Indicate whether the sound signal of the machine is set.
 * @param machine   The machine to probe.
 * @return          1 on an active sound, 0 else.
 */
int chip8_is_sound_active(chip8_t *machine);

/**
 * Print the state of the machine to stdout.
 * @param machine   The machine to inspect.
 */
void chip8_coredump(chip8_t *machine);

#endif /* _H_CHIP8_ */
