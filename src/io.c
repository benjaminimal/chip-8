#include <SDL2/SDL.h>

#include "io.h"

/* Internal definitions */

#define PI 3.14159265358979323846

// Represent a playable tone
typedef struct {
  float position;
  float increment;
} tone_data_t;


/* Globals */

static int _quit = 0;

static SDL_Window *_window = NULL;

static SDL_Renderer *_renderer = NULL;

static SDL_Texture *_texture = NULL;

static SDL_AudioDeviceID _device_id = 0;

static tone_data_t _tone;


/* Function prototypes */

static int _init_video();
static int _init_audio();
void _audio_callback(void *user_data, uint8_t *buffer, int bytes);
void _handle_key_change(chip8_t *machine, SDL_Event event);


/* Interface function definitions */

int io_init() {

  // Initialize SDL
  if (SDL_Init(SDL_INIT_TIMER | SDL_INIT_EVENTS | SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
    return -1;
  }

  // Set up video
  if (_init_video() < 0) { return -1; }

  // Set up audio
  if (_init_audio() < 0) { return -1; }

  return 0;
}

void io_cleanup() {
  // Clean up video
  if (_window != NULL) { SDL_DestroyWindow(_window); }
  if (_renderer != NULL) { SDL_DestroyRenderer(_renderer); }
  if (_texture != NULL) { SDL_DestroyTexture(_texture); }

  // Clean up audio
  if (_device_id != 0) { SDL_CloseAudioDevice(_device_id); }

  // CLean up SDL
  SDL_Quit();
}

const char *io_get_error() {
  return SDL_GetError();
}

int io_quit_requested() {
  return _quit;
}

void io_render_display(chip8_t *machine) {
  // Update texture
  SDL_UpdateTexture(
      _texture,
      NULL,
      machine->display,
      C8_DSP_WDT * sizeof(uint8_t) * C8_DSP_BPP
  );

  // Clear the renderer
  SDL_RenderClear(_renderer);

  // Draw the texture
  SDL_RenderCopy(_renderer, _texture, NULL, NULL);

  // Update the screen
  SDL_RenderPresent(_renderer);
}

void io_set_sound(int on) {
  SDL_PauseAudioDevice(_device_id, on ? 0 : 1);
}

void io_handle_events() {
  SDL_Event event;
  while(SDL_PollEvent(&event)) {
    switch (event.type) {
      case SDL_QUIT:
        _quit = 1;
        break;
    }
  }
}

static const SDL_Scancode keymap[0x10] = {
  SDL_SCANCODE_X,
  SDL_SCANCODE_1,
  SDL_SCANCODE_2,
  SDL_SCANCODE_3,
  SDL_SCANCODE_Q,
  SDL_SCANCODE_W,
  SDL_SCANCODE_E,
  SDL_SCANCODE_A,
  SDL_SCANCODE_S,
  SDL_SCANCODE_D,
  SDL_SCANCODE_Z,
  SDL_SCANCODE_C,
  SDL_SCANCODE_4,
  SDL_SCANCODE_R,
  SDL_SCANCODE_F,
  SDL_SCANCODE_V
};
int io_get_key_state(uint8_t key) {
  if (key >= C8_KEY_BND) { return 0; }
  const uint8_t *state = SDL_GetKeyboardState(NULL);
  return state[keymap[key]];
}


/* Internal function definitions */

/**
 * Open a window and enable drawing to it.
 * @return  0 on success, < 0 else.
 */
static int _init_video() {
  // Create window
  _window = SDL_CreateWindow(
      IO_VIDEO_WIN_TITLE,
      SDL_WINDOWPOS_UNDEFINED,
      SDL_WINDOWPOS_UNDEFINED,
      IO_VIDEO_WIN_WIDTH,
      IO_VIDEO_WIN_HEIGHT,
      SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE
  );
  if (_window == NULL) { return -1; }

  // Create renderer
  _renderer = SDL_CreateRenderer(
      _window,
      -1,
      SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC
  );
  if (_renderer == NULL) { return -1; }

  // Create texture
  _texture = SDL_CreateTexture(
      _renderer,
      SDL_PIXELFORMAT_RGB24,
      SDL_TEXTUREACCESS_STREAMING,
      C8_DSP_WDT,
      C8_DSP_HGT
  );
  if (_texture == NULL) { return -1; }

  return 0;
}

/**
 * Open sound device.
 * @return  0 on success, < 0 else.
 */
static int _init_audio() {
  // Set up tone data
  _tone.position = 0;
  _tone.increment = 2 * PI * IO_AUDIO_HZ / IO_AUDIO_FREQ;

  // Define audio spec
  SDL_AudioSpec want;
  want.freq = IO_AUDIO_FREQ;
  want.format = AUDIO_U8;
  want.channels = IO_AUDIO_CHANNELS;
  want.samples = IO_AUDIO_SAMPLES;
  want.callback = &_audio_callback;
  want.userdata = &_tone;

  // Open audio device
  SDL_AudioSpec have;
  _device_id = SDL_OpenAudioDevice(
      NULL,
      0,
      &want,
      &have,
      SDL_AUDIO_ALLOW_FORMAT_CHANGE
  );

  if (_device_id <= 0) {
    return -1;
  }

  return 0;
}

/**
 * The callback used by SDL to fill the audio buffer.
 */
void _audio_callback(void *user_data, uint8_t *buffer, int bytes) {
  tone_data_t *tone = (tone_data_t*) user_data;
  for (int i = 0; i < bytes; i++) {
    buffer[i] = sinf(tone->position) + 127;
    tone->position += tone->increment;
  }
}
