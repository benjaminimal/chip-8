#ifndef _H_IO_
#define _H_IO_

#include "chip8.h"

/* Constants */

/* Audio */
#define IO_AUDIO_AMPLITUDE  7000
#define IO_AUDIO_FREQ       4410
#define IO_AUDIO_CHANNELS   1
#define IO_AUDIO_SAMPLES    128
#define IO_AUDIO_HZ         1000

/* Video */
#define IO_VIDEO_WIN_TITLE    "CHIP-8"
#define IO_VIDEO_WIN_WIDTH    (C8_DSP_WDT * 10)
#define IO_VIDEO_WIN_HEIGHT   (C8_DSP_HGT * 10)


/* Functions */

/**
 * Initialize data which are needed for all io components. Has to be called
 * exactly once before using those.
 * @return  0 on success, < 0 else.
 */
int io_init();

/**
 * Deconstruct all data needed by the io module. Call when done with io
 * operations. Can safely be called multiple times.
 */
void io_cleanup();

/**
 * Get a string representatio of the last error from the io module. Only use
 * when an io function returns a negative value.
 * @return  A pointer to the message string.
 */
const char *io_get_error();

/**
 * Indicate whether the user has reqeusted to quit the application.
 * @return  1 on quit, else 0.
 */
int io_quit_requested();

/**
 * Draw the contents of the machines display to the current window.
 * @param machine   The machine whose display should be rendered.
 */
void io_render_display(chip8_t *machine);

/**
 * En- or disable sound.
 * @param on    1 to turn sound on, 0 to turn int off.
 */
void io_set_sound(int on);

/**
 * Handle events which are not related to the Chip-8 keypad, e.g. the user
 * wanting to close the window.
 * @param machine   The currently running Chip-8 machine.
 */
void io_handle_events();

/**
 * Check in what state a Chip-8 keypad key is.
 * @param key   The number of the Chip-8 keypad key, 0 <= key < 16.
 * @return      1 if the key is in down state, 0 else.
 */
int io_get_key_state(uint8_t key);

#endif /* _H_IO_ */
