#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <SDL2/SDL.h>

#include "chip8.h"
#include "io.h"

/**
 * TODO
 * We need to come up with a solution for the following
 *  - Handling run time errors (unknown opcode, read out of bounds, ...)
 *  - figure out why the screen is flashing on window create and destory
 *  - check out if collision detection is bugged
 */

/* Constants */

#define CYCLES_PER_SEC    1000
#define UPDATE_FREQ       60
#define CYCLES_PER_UPDATE (CYCLES_PER_SEC / UPDATE_FREQ)
#define UPDATE_TIME       (1.0f / (float) UPDATE_FREQ)

/* Macros to be used in main */

#define LOG_ERROR(...) \
  fprintf(stderr, "%s: ", argv[0]); fprintf(stderr, __VA_ARGS__)
#define EXIT_ERROR(...) \
  LOG_ERROR(__VA_ARGS__); \
  exit(1);
#define USAGE() EXIT_ERROR("USAGE: %s ROM\n", argv[0]);

#define GET_REMAINING_TIME(past, current) \
    (((past) - _get_seconds_elapsed(current, SDL_GetPerformanceCounter())) * 1000 - 1)
#define WAIT(past, duration) \
    if ((duration) > 0) { SDL_Delay(duration); } \
    while(_get_seconds_elapsed(past, SDL_GetPerformanceCounter()) < UPDATE_TIME) { }


/* Function prototypes */

void debug_machine(chip8_t *machine);
int load_rom_file(chip8_t *machine, char *rom_path);
float _get_seconds_elapsed(uint64_t last_time, uint64_t current_time);


/* The one and only entry point */

int main(int argc, char *argv[]) {

  // Check cli args
  if (argc != 2) {
    USAGE();
  }

  // Initialize the machine
  chip8_t machine;
  chip8_init(&machine, &io_get_key_state);

#ifdef DEBUG
  debug_machine(&machine);
#endif /* DEBUG */

  // Load the program to execute
  if (load_rom_file(&machine, argv[1]) < 0) {
    EXIT_ERROR("%s: error while loading rom %s\n", argv[0], argv[1]);
  }

  // Initialize io
  if (io_init() < 0) {
    io_cleanup();
    chip8_destroy(&machine);
    EXIT_ERROR("%s: error while initializing io: %s\n", argv[0], io_get_error());
  }

  // Intial draw
  io_render_display(&machine);

#ifdef DEBUG
  debug_machine(&machine);
#endif /* DEBUG */

  // Loop until interrupted
  uint64_t time_counter = SDL_GetPerformanceCounter();
  while (!io_quit_requested()) {
    // Check for events
    io_handle_events();

    // Throttle the emulation
    int step_count = CYCLES_PER_UPDATE;
    while (step_count-- > 0) {
      // Execute instruction
      if (chip8_step(&machine) != 0) {
        LOG_ERROR("unknown opcode\n");
      }
    }
    // Check time left to wait and wait
    int sleep_time = GET_REMAINING_TIME(UPDATE_TIME, time_counter);
    WAIT(time_counter, sleep_time);

    // Redraw screen
    io_render_display(&machine);

    // Decrement active timers
    chip8_update_timers(&machine);

    // Turn sound on or off
    io_set_sound(chip8_is_sound_active(&machine));

#ifdef DEBUG
    debug_machine(&machine);
#endif /* DEBUG */

    time_counter = SDL_GetPerformanceCounter();
  }


  // Clean up
  io_cleanup();
  chip8_destroy(&machine);

  exit(0);
}


/* Internal function definitions */

/**
 * Open a rom file and load it into memory to be executed.
 * @param machine   The Chip-8 machine to execute the rom.
 * @param rom_path  The path name of the rom file.
 * @return  0 on success, < 0 else.
 */
int load_rom_file(chip8_t *machine, char *rom_path) {
  // Open file to load
  FILE *rom = fopen(rom_path, "r");
  if (rom == NULL) { return -1; }

  // How much to read
  long rom_size;
  if (fseek(rom, 0, SEEK_END) != 0) {
    fclose(rom);
    return -1;
  }
  if ((rom_size = ftell(rom)) < 0) {
    fclose(rom);
    return -1;
  }
  rewind(rom);

  // Copy the rom to memory
  uint8_t *buffer = malloc(rom_size * sizeof(uint8_t));
  if (buffer == NULL) {
    fclose(rom);
    return -1;
  }
  if (fread(buffer, 1, rom_size, rom) < rom_size) {
    fclose(rom);
    free(buffer);
    return -1;
  }
  fclose(rom);

  // Load it into the machines memory
  if (chip8_load(machine, buffer, rom_size) < 0) {
    free(buffer);
    return -2;
  }

  free(buffer);
  return 0;
}

/**
 * Use to go step wise throug the execution while viewing the machines state.
 */
void debug_machine(chip8_t *machine) {
  chip8_coredump(machine);
  printf("Press [Enter] to continue...");
  getchar();
}

/**
 * Mease elapsed time between two counters.
 * @param last_time     The previous SDL high resolution counter.
 * @param current_time  The current SDL high resolution counter.
 * @return              The elapsed time in seconds.
 */
float _get_seconds_elapsed(uint64_t last_time, uint64_t current_time) {
  return (((float) (current_time - last_time)) / (float) SDL_GetPerformanceFrequency());
}
